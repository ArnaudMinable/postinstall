#! /bin/bash

groupadd ssh-users
usermod -a -G ssh-users root

echo "AllowGroups ssh-users" >> /etc/ssh/sshd_config

echo \
'APT::Install-Recommends "0";
APT::Install-Suggests "0";'>/etc/apt/apt.conf.d/60recommends

echo \
'deb http://deb.debian.org/debian/ stretch main
deb http://deb.debian.org/debian/ stretch-updates main
deb http://deb.debian.org/debian-security/ stretch/updates main'>/etc/apt/sources.list.d/debian.list

apt update && apt upgrade -y && apt autoremove -y && apt clean
apt install unattended-upgrades needrestart -y

